# -*- encoding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='frams_parser',
    version='0.1',
    author='Mateusz Kurek',
    author_email='master.mateusz@gmail.com',
    description="Reader and writer of framsticks files",
    url='http://www.framsticks.com/',
    keywords='',
    platforms=['any'],
    license='Apache Software License v2.0',
    packages=find_packages('src'),
    include_package_data=True,
    package_dir={'': 'src'},
    zip_safe=False,  # because templates are loaded from file path
    install_requires=[],
    entry_points={
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows :: Windows NT/2000',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP',
    ]
)
