# -*- encoding: utf-8 -*-
# according to DTD: https://www.framsticks.com/svn/framsticks/java/FramclipsePlugin/src/main/resources/framscript.dtd

import json
import logging
import operator
import os
import re
import xml.etree.ElementTree as ET
from collections import OrderedDict
from copy import deepcopy
from decimal import Decimal, InvalidOperation
from enum import IntEnum
from reprlib import recursive_repr as _recursive_repr

XML_PATH = 'framscript.xml'  # path to XML types definition
# mapping from xml types to Pyhton types
TYPES = {
    'string': str,
    'int': int,
    'integer': int,
    'float': Decimal,
    'untyped': str,
    '_default': str,
}
MIN_MAX_TYPES = {
    'integer': int,
    'float': Decimal,
    'string': int,
    '_default': int,
}
# global dict storing all types as Python classes
CLASSES = {}

logger = logging.getLogger(__name__)


class FramsObject(dict):
    """
    Attribute dict. Used to attribute access to dict
    """
    def __init__(self, *args, **kwargs):
        self.__dict__ = self
        super(FramsObject, self).__init__(*args, **kwargs)


class EmptyValue(object):
    """
    Empty value class acting like singleton-null value (other than None)
    """
    def __deepcopy__(self, *args, **kwargs):
        return self

    def __repr__(self):
        return 'NULL'

    def __str__(self):
        return 'NULL'

NULL = EmptyValue()


class DefaultValueError(Exception):
    pass


class DontSaveValueError(Exception):
    pass


class ParamReadonlyOrConstError(Exception):
    pass


class Flags(IntEnum):
    PARAM_READONLY = 1
    PARAM_DONTSAVE = 2
    # PARAM_SETLEVEL = 4
    # PARAM_LEVEL = 8
    PARAM_USERREADONLY = 16
    # PARAM_USERHIDDEN = 32
    # MUTPARAM_ALLOCENTRY = 64
    # MUTPARAM_ALLOCDATA = 128
    # PARAM_NOSTATIC = 256
    PARAM_CONST = 512
    # PARAM_CANOMITNAME = 1024
    PARAM_DONTLOAD = 2048
    # PARAM_NOISOLATION = 4096
    # PARAM_DEPRECATED = 8192


class FramsElement(object):
    """
    Single Framsticks Element (corresponds to <element> node in XML schema)
    """
    def __init__(self, *args, **kwargs):
        self._type = None
        self.type = str
        self.name = None
        self.max = None
        self.min = None
        self.default = NULL  # default value is NULL (to distinguish it from None)
        self.function = False
        self.value = NULL
        self._readonly = False
        self._dontsave = False
        self._const = False
        self._dontload = False
        self.flags = 0

        for attr, val in kwargs.items():
            setattr(self, attr, val)
        self._type = self.type
        self.type = TYPES.get(self.type, TYPES['_default'])
        self.value = NULL
        # map min and max to element type
        min_max_type = MIN_MAX_TYPES.get(self._type, MIN_MAX_TYPES['_default'])
        for attr in ('min', 'max'):
            val = getattr(self, attr)
            if val is not None:
                val = self.parse_value(val, type_=min_max_type)
                setattr(self, attr, val)
        if self.min and self.max and self.min > self.max:
            self.min = None
            self.max = None

        # if default is specified, cast it to element type too
        if self.default is not NULL:
            self.default = self.parse_value(self.default)
            self.default = self._validate_value(self.default)
        # set flags
        self.flags = int(self.flags)
        if self.flags & (Flags.PARAM_READONLY | Flags.PARAM_USERREADONLY):
            self._readonly = True
        if self.flags & (Flags.PARAM_DONTSAVE):
            self._dontsave = True
        if self.flags & (Flags.PARAM_CONST):
            self._const = True
        if self.flags & (Flags.PARAM_DONTLOAD):
            self._dontload = True

    def parse_value(self, value, type_=None):
        """
        Parse single (not serialized) value. Int is treated in special way
        to handle all cases (ex. +1, 1e10 etc)
        """
        type_ = type_ or self.type
        try:
            return type_(value)
        except ValueError:
            if type_ is int:
                try:
                    # try figure out base (2, 8, 10 or 16)
                    return type_(value, 0)
                except ValueError:
                    try:
                        # int does not support scientific format directly
                        return int(float(value))
                    except ValueError:
                        raise
            raise
        except InvalidOperation as e:
            raise ValueError(e)

    def _validate_value(self, value, skip_readonly=False):
        if not isinstance(value, self.type) and self._type not in ['untyped'] and value is not NULL:
            raise ValueError()
        if not skip_readonly and (self._readonly or self._const):
            raise ParamReadonlyOrConstError()
        # check min/max conditions
        if self.min:
            if (isinstance(value, (int, Decimal, float)) and value < self.min):
                logger.warning(
                    "Setting '{} = {}' exceeded allowed range (to small). Adjusted to {}".format(
                        self.name,
                        value,
                        self.min,
                    )
                )
                value = self.min
        if self.max:
            if (isinstance(value, str) and len(value) > self.max):
                logger.warning(
                    "Setting '{} = {}' exceeded allowed range (to big). Adjusted to {}".format(
                        self.name,
                        value,
                        value[:self.max],
                    )
                )
                value = value[:self.max]
            if (isinstance(value, (int, Decimal, float)) and value > self.max):
                logger.warning(
                    "Setting '{} = {}' exceeded allowed range (to big). Adjusted to {}".format(
                        self.name,
                        value,
                        self.max,
                    )
                )
                value = self.max
        return value

    def set_value(self, value, skip_readonly=False):
        """
        Set element value.

        Notice that value has to have valid type!
        """
        logger.debug('setting value |{}| for attr {}'.format(value, self.name))
        value = self._validate_value(value, skip_readonly)
        # additional parsing for multiline string (unescape ~)
        if isinstance(value, str) and '\n' in value:
            value = value.replace('\~', '~')
        # check if value has 'untyped' type and use serialized parser
        if self._type == 'untyped' and isinstance(value, str):
            value = self._parse_serialized(value)
        self.value = value

    def clear_value(self):
        self.value = NULL

    def _parse_serialized(self, value):
        """
        Parse serialized value and references using json.
        """
        if value.startswith('@Serialized:'):
            value = value[len('@Serialized:'):]
        try:
            # ast.literal_eval could be alternative to json
            # parsed_val = ast.literal_eval(value)
            # return parsed_val
            return json.loads(value, parse_float=Decimal)
        except (SyntaxError, ValueError):
            # try to replace references ^X by "^X"
            value = re.sub(r'(\^\d+)', r'"\1"', value)
            try:
                parsed = json.loads(value, parse_float=Decimal)

                # collect and resolve references
                def parse_references(root, references):
                    if isinstance(root, list):
                        references.append(root)
                        for el in root:
                            parse_references(el, references)
                    elif isinstance(root, dict):
                        references.append(root)
                        for el in root.values():
                            parse_references(el, references)

                def resolve_references(root, references, ancestors):
                    if isinstance(root, list):
                        ancestors.append(root)
                        for i, el in enumerate(root):
                            root[i] = resolve_references(
                                el,
                                references,
                                ancestors
                            )
                        ancestors.pop()
                    elif isinstance(root, dict):
                        ancestors.append(root)
                        for k, v in root.items():
                            root[k] = resolve_references(
                                v,
                                references,
                                ancestors
                            )
                        ancestors.pop()
                    elif isinstance(root, str):
                        ref = re.match(r'\^(\d+)', root)
                        if ref:
                            try:
                                key = int(ref.groups()[0])
                                val = references[key]
                                # if val not in ancestors:
                                root = val
                            except (KeyError, ValueError):
                                pass
                    return root

                if isinstance(parsed, (list, dict)):
                    references = []
                    parse_references(parsed, references)
                    resolve_references(parsed, references, [])

                return parsed
            except Exception:
                pass
        return value

    def get_text_representation(self, skip_default=False, escape=True):
        """
        Return text representation of element (ready to save).
        """
        value = self.value
        if value == self.default and skip_default:
            raise DefaultValueError()
        if self._dontsave:
            raise DontSaveValueError()
        if value is NULL and not skip_default and self.default is not NULL:
            value = self.default
        if value is NULL:
            raise DefaultValueError()
        if isinstance(value, str):
            if '\n' in value and escape:
                # escape ~ in multiline string
                value = '~\n' + value.replace('~', '\~') + '~'
        elif (isinstance(value, (list, dict)) or value is None) and escape:
            try:
                value = '@Serialized:' + json.dumps(
                    value,
                    separators=(',', ':'),
                    cls=DecimalEncoder,
                )
            except ValueError:
                value = "@Serialized:" + str(value).replace("'", '"')
        elif not escape:
            if value is None:
                value = "null"
        return str(value)

    def __repr__(self):
        return repr(self.value)

    def __str__(self):
        return str(self.value)


# initialize operators
def apply_operator(func):
    def inner(self, other):
        return func(self.value, other)
    return inner

for op in operator.__all__:
    setattr(
        FramsElement,
        '__{}__'.format(op.replace('_', '')),
        apply_operator(getattr(operator, op))
    )


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)


class FramsType(object):
    """
    Framsticks type base class (corresponds to <type> node in XML schema)
    """
    _name = None  # type name
    _context = None  # type context
    # set to True to allow all attributes (elements), beside schema
    _allow_all_attributes = False
    _valid_attributes = OrderedDict()  # valid attributes according to schema
    __ignore_invalid = False
    __skip_default = True

    def __init__(self, kwargs=None):
        # make copy of valid attributes
        self.__items = deepcopy(self._valid_attributes)
        if kwargs:
            # try to parse each variable according to it's type
            self._assign_init(kwargs)

    def _assign_init(self, kwargs):
        # assign each attribute value to self
        # (call _set_attr underneath for each attribute)
        self.__ignore_invalid = True
        try:
            for attr, v in kwargs.items():
                self[attr] = v
        finally:
            self.__ignore_invalid = False

    def get_text_representation(self, skip_default=False, separator=':', escape=True):
        """
        Return text representation of Framsticks type instance (ready to save)

        :param skip_default: if True and attribute (element) has default value
            assigned, it's omited
        :type skip_default: bool
        """
        self.__skip_default = skip_default
        # start with type name followed by :
        result = [self._name + ':']
        # store each attribute
        for attr, value in self.items():
            # call __items directly to get all values
            element = self.__items[attr]
            try:
                # get text_representation of single element
                text_repr = element.get_text_representation(skip_default, escape=escape)
                result.append(separator.join((attr, text_repr)))
            except (DefaultValueError, DontSaveValueError):
                pass
        self.__skip_default = True
        return '\n'.join(result)

    def _set_attr(self, attr, value):
        """
        Set attribute (element) value in type instance
        """
        # apply additional logic only for attributes which not start with _
        if not attr.startswith('_'):
            # check if attribute name is valid with this type
            if attr not in self.__items and not self._allow_all_attributes:
                if self.__ignore_invalid:
                    return
                raise AttributeError('Invalid attribute: {}'.format(attr))
            # get FramsElement associatied with attribute
            try:
                element = self.__items[attr]
            except KeyError:
                # create new if not exist
                element = FramsElement(name=attr, type='untyped')
                self.__items[attr] = element
            # cast value to element type
            # (cast before set it in element, because element accept only valid
            # variable types)
            try:
                value = element.parse_value(value)
            except ValueError:
                logger.warning(
                    "Could not parse '{}'".format(value)
                )
                value = element.default
            except Exception:
                logger.exception('Exception during processing attr {} with value {}'.format(attr, value))
                raise
            element.set_value(value, skip_readonly=self.__ignore_invalid)
        else:
            return super(FramsType, self).__setattr__(attr, value)

    def _get_attr(self, attr):
        """
        Return attribute from __items
        """
        if attr in self.__items:
            val = self.__items[attr]
            if val is NULL:
                return None
            return val
        return AttributeError()

    def _del_attr(self, attr):
        if attr in self.__items:
            del self.__items[attr]
        else:
            raise AttributeError()

    # HELPERS

    def __setattr__(self, attr, value):
        return self._set_attr(attr, value)

    def __setitem__(self, attr, value):
        return self._set_attr(attr, value)

    def __getattr__(self, attr):
        return self._get_attr(attr)

    def __getitem__(self, attr):
        return self._get_attr(attr)

    def __delattr__(self, attr):
        return self._del_attr(attr)

    def __delitem__(self, attr):
        return self._del_attr(attr)

    def __dir__(self):
        return self.keys()

    @_recursive_repr()
    def __repr__(self):
        try:
            return json.dumps(
                OrderedDict([
                    (k, v.value) for (k, v) in self.items()
                ]),
                indent=4,
                cls=DecimalEncoder,
            )
        except ValueError:
            return str(dict(self.items()))

    def __iter__(self):
        for key in self.__items:
            # return only not-NULL values (None is valid here)
            if (self.__items[key].value is not NULL) or not self.__skip_default:
                yield key

    def keys(self):
        return iter(self)

    def values(self):
        for k in self:
            yield self[k]

    def items(self):
        for k in self:
            yield (k, self[k])


def create_types():
    """
    Create types from XML schema
    """
    xml_path = os.path.join(os.path.dirname(__file__), XML_PATH)
    xml_tree = ET.parse(xml_path)
    # find all type nodes
    for t in xml_tree.findall('type'):
        name = t.attrib['name'].title()
        logger.debug('processing ', name)
        # get all attributes of type
        attrs = {'_{}'.format(k): v for (k, v) in t.attrib.items()}
        attrs.update({'_valid_attributes': OrderedDict()})
        type_ = type(name, (FramsType, ), attrs)
        # get type elements
        for el in t.getchildren():
            if el.tag == 'element':
                el_inst = FramsElement(**el.attrib)
                if not el_inst._dontload:
                    type_._valid_attributes[el.attrib['name']] = el_inst
        # assign type to globals (to make it available to
        # `from objects import ...`) and CLASSES dict
        globals()[name] = type_
        CLASSES[name.lower()] = type_


def fix_classes():
    """
    Fix and add some additional types
    """
    # map genotype to org
    org_type = type('Org', (Genotype, ), {'_name': 'org'})
    globals()['Org'] = org_type
    # create dummy (universal) type which will allow any attribute
    dummy_type = type('Dummy', (FramsType, ), {
        '_name': 'unknown',
        '_allow_all_attributes': True
    })
    globals()['Dummy'] = dummy_type

    CLASSES.update({
        'org': org_type,
        'dummy': dummy_type,
    })

create_types()
fix_classes()
