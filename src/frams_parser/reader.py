# -*- encoding: utf-8 -*-
import logging
from collections import OrderedDict

from frams_parser.objects import CLASSES


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class FramsReader(object):
    """
    Reader for Framsticks files.
    """
    def __init__(self, filepath):
        self.filepath = filepath
        self.print_object = False

    def read(self):
        """
        Read and parse Framsticks file.
        """
        objects = []
        with open(self.filepath, 'r') as f:
            attributes = OrderedDict()
            multiline = False
            multiline_field = None
            multiline_value = []

            for line in f:
                line = line.strip()
                if line.startswith('#') and not multiline:
                    continue
                # multiline
                elif line.endswith('~') and not line.endswith('\~'):
                    if not multiline:
                        multiline_field, value = line.split(':')
                        if len(value.strip()) > 1:
                            # strip ~ at the end
                            multiline_value.append(value.strip()[:-1])
                        multiline = True
                    else:
                        if len(line.strip()) > 1:
                            multiline_value.append(line[:-1])
                        attributes[multiline_field.strip()] = "\n".join(
                            multiline_value
                        )
                        multiline = False
                        multiline_field = None
                        multiline_value = []
                elif multiline:
                    multiline_value.append(line)
                elif not line and not multiline:
                    if attributes:
                        print('----- object found, loading...')
                        objects.append(
                            self._create_object_instance(attributes)
                        )
                        print('-----\n\n')
                        attributes = OrderedDict()
                else:
                    if not attributes:
                        attributes['_type'] = line[:-1]
                    field, value = line.split(':', 1)
                    attributes[field.strip()] = value.strip()

            if attributes:
                objects.append(self._create_object_instance(attributes))
        return objects

    def _create_object_instance(self, attributes):
        """
        Create object instance from attributes dict.
        """
        type_ = attributes.pop('_type')
        try:
            cls = CLASSES[type_]
        except KeyError:
            # use dummy type if not found in schema
            cls = CLASSES['dummy']
        instance = cls(attributes)
        # save type name for dummy type
        if isinstance(instance, CLASSES['dummy']):
            instance._name = type_
        if self.print_object:
            text_representation = instance.get_text_representation(
                skip_default=False,
                separator='=',
                escape=False,
            )
            print(text_representation)
        return instance


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print('usage: python reader.py <input file>')
        sys.exit(1)
    fr = FramsReader(sys.argv[1])
    fr.print_object = True
    obj = fr.read()
    print(obj)
