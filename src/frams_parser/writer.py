# -*- encoding: utf-8 -*-


class FramsWriter(object):
    """
    Framsticks file writer (from FramsType instances).
    """
    def __init__(self, filepath):
        self.filepath = filepath

    def write(self, objects, skip_default=False):
        """
        Write objects text representation to file.

        :param objects: list of FramsType instances
        """
        with open(self.filepath, 'w') as f:
            for obj in objects:
                text_representation = obj.get_text_representation(
                    skip_default
                )
                f.write(text_representation)
                f.write('\n\n')


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 3:
        print('usage: python writer.py <input file> <output file>')
        sys.exit(1)
    from frams_parser.reader import FramsReader
    fr = FramsReader(sys.argv[1])
    obj = fr.read()
    fw = FramsWriter(sys.argv[2])
    fw.write(obj)
