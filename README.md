frams_parser
============

Introduction
------------

frams_parser is a Python 3 reader and writer of Framsticks files (*.gen/sim/neuro/expdef/script/style etc.).

Definitons of contexts and objects are taken from Framsticks xml files: https://www.framsticks.com/svn/framsticks/java/FramclipsePlugin/src/main/resources/

Installation
------------

The best way to use frams_parser is installing it in virtual environment (virtualenv).

```
pyvenv frams_parser  # Python 3.4 required!
cd frams_parser
source bin/activate  # activate virtual environment
```

To install frams_parser from sources:
```
mkdir src
cd src
git clone <frams_parser_repo_url>
cd frams_parser
pip install -e .
```

If you don't want to clone the whole repository, you can install directly from the control version system (inside virtualenv or in system packages):
```
pip install https://bitbucket.org/Matthew23/mioib_frams_parser/get/master.zip
```
Notice that after installing it in such a way, you will not be able to directly use `reader.py` and `writer.py` (only as an importable library).

If you don't want to install frams_parser (for example you just want to test it), you don't have to create any virtualenv etc -- frams_parser only uses Python standard library, so it should work out of the box after downloading source files.


Basic usage
-----------

```
cd src/frams_parser
python writer.py sample/flag.frams sample/flag.frams.out  # this will read input, parse it and write it to output file
```

Using frams_parser in your own project:
```
>>> from frams_parser import reader
>>> fr = reader.FramsReader('Framsticks/data/other.gen')  # convert this file to uft-8 or ascii first!
>>> objects = fr.read()
>>> print(objects[0])
{
    "name": "Bird",
    "genotype": "XXX(RRX(RRX(X, X), , ), RRX(, X(X, X)), RRX(RRX(X, X), , ))",
    "info": "HOW CREATED:\nDesigned\nSUBMITTED BY: Szymon Ulatowski, 1998-00-00\nlooks like a bird",
    "num": 97,
    "popsiz": 0
}
>>> print(objects[0].info)
HOW CREATED:
Designed
SUBMITTED BY: Szymon Ulatowski, 1998-00-00
looks like a bird
>>> print(obj[1]['num'])
98
>>> obj[1]['num'] += 10
>>> print(obj[1]['num'])
108
```
Notice that you can use object attributes using the dict notation (`obj['attribute']`) or the regular attribute notation (`obj.attribute`).


Writing the result to a file:
```
>>> from frams_parser import writer
>>> wr = writer.FramsWriter('other.gen.out')
>>> wr.write(objects)
```